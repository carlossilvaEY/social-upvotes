Meteor.publish 'tabular_Invitations', (tableName, ids, fields) ->
  # logger.info "tabular_Jobs", tableName:tableName, ids:ids, fields:fields
  check tableName, String
  check ids, Array
  check fields, Match.Optional(Object)
  @unblock()

  return Invitations.find({_id: {$in: ids}});

Meteor.publish 'invitationsCounter', () ->
  Counts.publish this, 'invitationsCounter', Invitations.find()
  return
