Accounts.onCreateUser (options, user) ->
  userProperties =
    profile: options.profile || {}

  user = _.extend user, userProperties
  user

Meteor.methods
  deleteUser: (targetUserId) ->
    loggedInUser = Meteor.user()
    if !loggedInUser or !Roles.userIsInRole(loggedInUser, ['admin', 'staff'])
      throw new (Meteor.Error)(403, 'Access denied')

    targetUser = Meteor.users.findOne({_id:targetUserId})
    if !targetUser
      throw new (Meteor.Error)(402, 'This user does not exists')

    try
      userJobs = QueueJobs.remove({userId:targetUserId})
      Meteor.users.remove({_id:targetUserId})
      true
    catch error
      throw new (Meteor.Error)(403, error)

  validateCode: (code) ->
    return !!Invitations.findOne({reg_code: code, active:true})

  checkInvitationOnSignUp: () ->
    user = Meteor.user()
    if !user
      throw new (Meteor.Error)(402, 'This user does not exists')

    if user.profile.reg_code?
      throw new (Meteor.Error)(402, 'This user has not registration code')

    invitation = Invitations.findOne({reg_code: reg_code})

    if invitation
      Invitations.update({_id:invitation._id}, {$set:{active:false}})
      true
    else
      throw new Meteor.Error(403, "There is no invitation for this email")

  activeUser: (params) ->
    if !params.userId# or !params.paymentId
      throw new Meteor.Error(403, "Invalid params")

    user = Meteor.users.findOne params.userId

    if !user
      throw new Meteor.Error(403, "Invalid user")

    updateUser =
      $set:
        'profile.paymentId': params.paymentId

    Meteor.users.update({_id: user._id}, updateUser)
    Roles.addUsersToRoles user._id, ['subscribed']
    true
