@makeRequest = (requestOptions = {}, callback)->
  request = Meteor.npmRequire 'request'
  #unless _.isObject(requestOptions) and 'url' of requestOptions and 'cookie' of requestOptions
  #  console.log  "Please provide properly formatted request options, you provided #{requestOptions}"
  #  return false
    #logger.error 'wrong-request', "Please provide properly formatted request options, you provided '#{requestOptions}'"
    #return false

  url = requestOptions.url
  #check format url
  unless _.isString(url) and /^https?:\/\/(w{3})?\.?\S*(\.\w{2,})?\/?/i.test(url)
    logger.error 'wrong-url', "Please provide a properly formatted url, you provided '#{url}'"
    return false

  method = requestOptions.method or "GET"
  requestOptions['jar'] = request.jar()

  request requestOptions, (error, response) ->
    callback(error, response, requestOptions['jar'])

  return

@requestAsync = (requestOptions = {})->
  request = Meteor.npmRequire 'request'

  url = requestOptions.url
  unless _.isString(url) and /^https?:\/\/(w{3})?\.?\S*(\.\w{2,})?\/?/i.test(url)
    logger.error 'wrong-url', "Please provide a properly formatted url, you provided '#{url}'"
    return false

  requestOptions['jar'] = request.jar()

  response = Async.runSync (done) ->
    request requestOptions, (error, response) ->
      if error
        logger.warn "Makerequest: error", url: url
        result =
          statusCode: if response?.statusCode? then response.statusCode else 0
          data: null
          content: null
          headers: null
          cookieResponse: null
          error: error

      else
        result =
          statusCode: response.statusCode
          data: response
          content: response.body
          headers: response.headers
          cookieResponse: requestOptions['jar']
          error: null


      done(false, result)
      return

  return response.result
