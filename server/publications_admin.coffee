Meteor.publish 'tabular_Users', (tableName, ids, fields) ->
  check tableName, String
  check ids, Array
  check fields, Match.Optional(Object)
  @unblock()
  # requires meteorhacks:unblock package
  extra_fields =
    'roles': 1
  _.extend fields, extra_fields

  Counts.publish(this, 'usersCounter', Meteor.users.find(), { noReady: true })

  return Meteor.users.find({_id: {$in: ids}}, {fields: fields})



Meteor.publish 'tabular_Jobs', (tableName, ids, fields) ->
  # logger.info "tabular_Jobs", tableName:tableName, ids:ids, fields:fields
  check tableName, String
  check ids, Array
  check fields, Match.Optional(Object)
  @unblock()
  # requires meteorhacks:unblock package


  extra_fields =
    'retries': 1
    'depends': 1
    'resolved': 1
    'repeated': 1
    'repeats': 1
    'status': 1
    'progress':1
    'log': 1


  _.extend fields, extra_fields

  Counts.publish this, 'jobsCounter', QueueJobs.find()

  return QueueJobs.find({_id: {$in: ids}}, {fields: fields})
