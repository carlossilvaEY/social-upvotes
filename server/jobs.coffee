Meteor.startup ->
  logger.info "Server restarted"
  QueueJobs.startJobServer()



meneameUpVoteWorker = (job, callback) ->
  data = job.data

  logger.debug "meneameUpVoteWorker called", jobType:job.type, jobId:job.doc._id, jobData:job.data

  try
    job.progress 30, 100
    Meteor.call 'meneameUpvote', data.account, data.url, (error, response)->
      if error
        logger.warn "meneameUpVoteWorker: fail", error: error
      else
        logger.info "meneameUpVoteWorker: success", response: response

    job.done()
    job.remove()

  catch error
    logger.warn "meneameUpVoteWorker: fail", error:error
    job.fail("#{error}")
  callback null

QueueJobs.processJobs 'meneameUpVoteJob', { concurrency: 10, pollInterval: 2500 }, meneameUpVoteWorker

redditUpVoteWorker = (job, callback) ->
  data = job.data

  logger.debug "redditUpVoteWorker called", jobType:job.type, jobId:job.doc._id, jobData:job.data

  try
    job.progress 30, 100
    #console.log "account: ", account
    #console.log "url: #{url}"
    Meteor.call 'redditUpvote', data.account, data.url, (error, response)->
      if error
        logger.warn "redditUpVoteWorker: fail", error: error
      else
        logger.info "redditUpVoteWorker: success", response: response

    job.done()
    job.remove()

  catch error
    logger.warn "redditUpVoteWorker: fail", error:error
    job.fail("#{error}")
  callback null

QueueJobs.processJobs 'redditUpVoteJob', { concurrency: 10, pollInterval: 2500 }, redditUpVoteWorker
