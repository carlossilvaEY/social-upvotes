jobStatusCancellable = [ 'running', 'ready', 'waiting', 'paused' ]
jobStatusPausable = [ 'ready', 'waiting' ]
jobStatusRemovable = [ 'cancelled', 'completed', 'failed' ]
jobStatusRestartable = [ 'cancelled', 'failed' ]

Template.adminJobs.helpers
  jobEntries: () ->
    # Reactively populate the table
    this.find({})

Template.tabularJobActions.rendered = () ->
  this.$('.button-column').tooltip({selector: 'button[data-toggle=tooltip]'})

Template.tabularJobActions.events
  'click .cancel-job': (e, t) ->
    console.log "Cancelling job: #{this._id}", t
    # job = Template.currentData()
    QueueJobs.getJob this._id, (error, job) ->
      if error
        console.log "error", error
      else
        job.cancel() if job

  'click .remove-job': (e, t) ->
    console.log "Removing job: #{this._id}"
    # job = Template.currentData()
    QueueJobs.getJob this._id, (error, job) ->
      if error
        console.log "error", error
      else
        job.remove() if job

  'click .restart-job': (e, t) ->
    console.log "Restarting job: #{this._id}"
    # job = Template.currentData()
    QueueJobs.getJob this._id, (error, job) ->
      if error
        console.log "error", error
      else
        job.restart() if job
  'click .rerun-job': (e, t) ->
    console.log "Rerunning job: #{this._id}"
    # job = Template.currentData()
    QueueJobs.getJob this._id, (error, job) ->
      if error
        console.log "error", error
      else
        job.rerun({ wait: 15000 }) if job
  'click .pause-job': (e, t) ->
    console.log "Pausing job: #{this._id}"
    # job = Template.currentData()
    QueueJobs.getJob this._id, (error, job) ->
      if error
        console.log "error", error
      else
        job.pause() if job

  'click .resume-job': (e, t) ->
    console.log "Resuming job: #{this._id}"
    # job = Template.currentData()
    QueueJobs.getJob this._id, (error, job) ->
      if error
        console.log "error", error
      else
        job.resume() if job

  'click .show-job-log': (e, t) ->
    console.log "Showing job log: #{this._id}"
    $('#job-log-modal').modal('show')
    $('#job-log-modal').find('.modal-title').text("Log Info:#{this._id}")


    table = Blaze.toHTMLWithData(Template.tabularJobLogTable, this)

    $('#job-log-modal').find('.modal-body').html(table)

# Template.jobEntry.helpers
#   futurePast: () ->
#     Session.get 'date'
#     if this.after > new Date()
#       "text-danger"
#     else
#       "text-success"



Template.jobControls.events
  'click .clear-completed': (e, t) ->
    console.log "clear completed"
    ids = t.data.find({ status: 'completed' },{ fields: { _id: 1 }}).map (d) -> d._id
    console.log "clearing: #{ids.length} jobs"
    t.data.removeJobs(ids) if ids.length > 0

  'click .pause-queue': (e, t) ->
    if $(e.target).hasClass 'active'
      console.log "resume queue"
      ids = t.data.find({ status: 'paused' },{ fields: { _id: 1 }}).map (d) -> d._id
      console.log "resuming: #{ids.length} jobs"
      t.data.resumeJobs(ids) if ids.length > 0
    else
      console.log "pause queue"
      ids = t.data.find({ status: { $in: t.data.jobStatusPausable }}, { fields: { _id: 1 }}).map (d) -> d._id
      console.log "pausing: #{ids.length} jobs"
      t.data.pauseJobs(ids) if ids.length > 0

  'click .stop-queue': (e, t) ->
    unless $(e.target).hasClass 'active'
      console.log "stop queue"
      t.data.stopJobs()
    else
      console.log "restart queue"
      t.data.stopJobs(0)

  'click .cancel-queue': (e, t) ->
    console.log "cancel all"
    ids = t.data.find({ status: { $in: t.data.jobStatusCancellable } }).map (d) -> d._id
    console.log "cancelling: #{ids.length} jobs"
    t.data.cancelJobs(ids) if ids.length > 0

  'click .restart-queue': (e, t) ->
    console.log "restart all"
    ids = t.data.find({ status: { $in: t.data.jobStatusRestartable } }).map (d) -> d._id
    console.log "restarting: #{ids.length} jobs"
    t.data.restartJobs(ids, (e, r) -> console.log("Restart returned", r)) if ids.length > 0

  'click .remove-queue': (e, t) ->
    console.log "remove all"
    ids = t.data.find({ status: { $in: t.data.jobStatusRemovable } }).map (d) -> d._id
    console.log "removing: #{ids.length} jobs"
    t.data.removeJobs(ids) if ids.length > 0


Template.tabularJobLogTable.helpers
  showData: () ->
    # console.log "showData"
    JSON.stringify(this.data)

Template.tabularJobStatus.helpers
  running: () ->
    if Template.instance().view.isRendered
      # This code destroys Bootstrap tooltips on existing buttons that may be
      # about to disappear. This is done here because by the time the template
      # autorun function runs, the button may already be out of the DOM, but
      # a "ghost" tooltip for that button can remain visible.
      Template.instance().$("button[data-toggle=tooltip]").tooltip('destroy')
    this.status is 'running'
  statusBG: () ->
    {
      waiting: 'primary'
      ready: 'info'
      paused: 'default'
      running: 'default'
      cancelled: 'warning'
      failed: 'danger'
      completed: 'success'
    }[this.status]

Template.tabularJobActions.helpers
  cancellable: () ->
    this.status in jobStatusCancellable

  removable: () ->
    this.status in jobStatusRemovable

  restartable: () ->
    this.status in jobStatusRestartable

  rerunable: () ->
    this.status is 'completed'

  pausable: () ->
    this.status in jobStatusPausable

  resumable: () ->
    this.status is 'paused'
