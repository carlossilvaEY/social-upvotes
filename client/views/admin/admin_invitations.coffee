
Template.tabularRegistrationCode.helpers
  'registration_link': () ->
    return "http://piratada.co/sign-up/?reg_code=#{@reg_code}"

Template.tabularRegistrationCode.events
  'click .generate_code': (e, t) ->
    e.preventDefault()

    token = Random.hexString(10)
    updateObject =
      $set: {reg_code: token}

    Invitations.update({_id:t.data._id}, updateObject)
