#Template.usersPage.helpers
#  'errorMessage': () ->
#    errors = Session.get("usernameSubmitErrors")
#    if errors
#      errors
#
#  'errorClass': () ->
#    if !!Session.get("usernameSubmitErrors") then "has-error" else ""
#
#Template.usersPage.onCreated ->
#  Session.set 'quoraUsers', []
#
#Template.usersPage.onRendered ->
#  qUsers = Meteor.user().profile.quoraUsers
#  if qUsers
#    Session.set 'quoraUsers', qUsers
#  Session.set 'usernameSubmitErrors', null
#
#Template.quoraUserForm.onRendered ->
#  $('.overlay').hide()
#
#Template.quoraUserForm.events
#  'submit form#addUsernameForm': (e) ->
#    e.preventDefault()
#    username = $(e.target).find("[name=username]").val()
#
#    if !username
#      return Session.set "usernameSubmitErrors", 'You have to write a username'
#
#    $("#submitUsername").attr("disabled", true)
#    $('.overlay').show()
#
#    Meteor.call 'checkAndAddUsername', username, (error, data) ->
#      $('.overlay').hide()
#      $("#submitUsername").attr("disabled", false)
#      $(e.target).find("[name=username]").val('')
#      if error
#        if error.error is 404
#          return Session.set "usernameSubmitErrors", 'Wrong username. Try again please'
#        else
#          return Session.set "usernameSubmitErrors", 'Network error. Try again'
#      else
#        qusers = Session.get 'quoraUsers'
#        qusers.push(data)
#        Session.set 'quoraUsers', qusers
#    return
#
#Template.quoraUsers.helpers
#  'quoraUsers': () ->
#    QuoraUsers.find()
#
#Template.quoraUsers.onCreated ->
#  @autorun =>
#    qusers = Session.get 'quoraUsers'
#    @subscribe "quoraUsers", qusers
#
#Template.quoraUser.onRendered ->
#  $(".info-help").tooltip()
#
#Template.quoraUser.events
#  'click .unfollowUser': (e, t) ->
#    e.preventDefault()
#    quoraUserId = t.data._id
#    if confirm("Are you sure you want to unfollow #{t.data.name}?")
#      qusers = Session.get 'quoraUsers'
#      qusers = _.without(qusers, quoraUserId)
#      Session.set 'quoraUsers', qusers
#      Meteor.call 'unfollowQuoraUser', quoraUserId, (error, quoraUserId) ->
#        if error
#          qusers.push(quoraUserId)
#          Session.set 'quoraUsers', qusers
#
