Template.servicesAccounts.helpers
  'selector': ()->
    return service: FlowRouter.getParam 'service'

  'service': () ->
    FlowRouter.getParam 'service'

Template.servicesAccountsActions.events
  'click .enable-account-toggle': (e, t) ->
    e.preventDefault()
    accountId = t.data._id
    active = t.data.active

    Meteor.call 'updateStateAccount', accountId, active, (error, data) ->
      if error
        Bert.alert "There was an error: #{error}", "danger"
      else
        Bert.alert "Account has been updated succesfully", "success"

  'click .delete-account': (e, t) ->
    e.preventDefault()
    accountId = t.data._id

    if confirm("Are you sure you want to delete this account")
      Meteor.call 'deleteAccount', accountId, (error, data) ->
        if error
          Bert.alert "Sorry, we got an error: #{error.reason}", "danger"
        else
          Bert.alert "Account delete succesfully", "success"
