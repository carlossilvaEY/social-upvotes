Template.upvoteDownvote.helpers
  'service': () ->
    FlowRouter.getParam 'service'

  'formType': ()->
    service = FlowRouter.getParam 'service'

    if service == services[0] or service == services[1]
      return 'simple'



Template.upDownSimple.events
  'submit form#addPrivateAccount': (e) ->
    e.preventDefault()

    service = FlowRouter.getParam 'service'

    url = $(e.target).find("[name=url]").val()
    upvotes = $(e.target).find("[name=upvotes]").val()
    downvotes = $(e.target).find("[name=downvotes]").val()

    Meteor.call "upvoteDownvote", url, upvotes, downvotes, service, (error, response)->
      if error
        return Bert.alert "Sorry, there was an error: #{error}", 'danger'
      else if response.error
        return Bert.alert "Sorry, there was an error: #{response.error}", 'danger'
      else
        return Bert.alert "Success, #{response.message}", 'success'

    return
