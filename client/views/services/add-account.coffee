Template.serviceAddUser.helpers
  'service': () ->
    FlowRouter.getParam 'service'

Template.serviceAddUser.events
  'submit form#addPrivateAccount': (e) ->
    e.preventDefault()

    service = FlowRouter.getParam 'service'

    username = $(e.target).find("[name=username]").val()
    password = $(e.target).find("[name=password]").val()
    proxy = $(e.target).find("[name=proxy]").val()

    Meteor.call "#{service}Login", username, password, (error, response)->
      if error
        return Bert.alert "Sorry, there was an error: #{error}", 'danger'
      else if response.error
        return Bert.alert "Sorry, there was an error: #{response.error}", 'danger'
      else
        console.log response.message
        return Bert.alert "Success, #{response.message}", 'success'

    return
