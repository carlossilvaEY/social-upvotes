Template.askInvitation.helpers
  'errorMessage': () ->
    errors = Session.get("invitationSubmitErrors")
    if errors
      errors

  'errorClass': () ->
    if !!Session.get("invitationSubmitErrors") then "has-error" else ""

Template.askInvitation.onRendered ->
  $('#thanksInvitation').hide()
  Session.set 'invitationSubmitErrors', null

Template.askInvitation.events
  'submit #at-invitation-form': (e) ->
    e.preventDefault()
    email = $(e.target).find("[name=at-field-email]").val()

    if !email
      return Session.set "invitationSubmitErrors", 'You have to provide a valid email'

    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if !filter.test(email)
      return Session.set "invitationSubmitErrors", 'Format email error!'

    params =
      email: email
      active: true
      createdAt: new Date()

    Meteor.call 'addInvitation', params, (error, data) ->
      if error
        return Session.set "invitationSubmitErrors", 'You have already asked an invitation with this email'
      else
        console.log "callback add invitation"
        console.log data
        
        $('#askInvitation').hide()
        $('#thanksInvitation').fadeIn()
        $(e.target).find("[name=at-field-email]").val('')
        $(e.target).find("[name=at-field-message]").val('')
