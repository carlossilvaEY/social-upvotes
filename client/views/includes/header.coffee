Template.header.onRendered ->

  $('[data-toggle=\'offcanvas\']').click (e) ->
    e.preventDefault()
    #If window is small enough, enable sidebar push menu
    if $(window).width() <= 992
      $("body").toggleClass('sidebar-open')
    else
      #Else, enable content streching
      $("body").toggleClass('sidebar-collapse')
    return
  return

Template.header.events
   'click #logout': ->
     AccountsTemplates.logout()
