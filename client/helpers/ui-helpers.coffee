UI.registerHelper('getImageUser', (userId) ->
  user = Meteor.users.findOne({_id: userId})
  if user
    "/images/avatar3.png"
)

UI.registerHelper('getUserEmail', (userId) ->
  user = Meteor.users.findOne({_id: userId})
  if user
    if user.emails
      user.emails[0].address
)

UI.registerHelper('getUserEmailVerified', (userId) ->
  user = Meteor.users.findOne({_id: userId})
  if user
    if user.emails[0]?
      user.emails[0].verified
)

UI.registerHelper('verifiedEmail', (userId) ->
  user = Meteor.users.findOne({_id: userId})
  if user
    if user.emails
      if user.emails[0].verified
        true
      else
        false
)
UI.registerHelper 'getYear', () ->
  moment().year()

UI.registerHelper 'getDate', () ->
  now = new Date()
  moment(now).format('YYYY/MM/DD')

UI.registerHelper('userHasEmail', (userId) ->
  user = Meteor.users.findOne({_id: userId})
  if user
    if user.emails
      if user.emails.length > 0
        true
      else
        false
)

UI.registerHelper 'getServiceActive', (service, userId) ->
  active = false
  user = Meteor.users.findOne({_id: userId})
  if !user
    user = Meteor.user()

  if user and user.profile[service]?
    active = user.profile[service].active
  active


UI.registerHelper 'getServiceRole', (service, userId) ->
  role = null
  user = Meteor.users.findOne({_id: userId})
  if !user
    user = Meteor.user()
  if user and user.profile[service]?
    role = user.profile[service].role
  if !role
    role = user.roles[0]
  role

UI.registerHelper "prettifyDate", (timestamp) ->
  moment(new Date(timestamp)).fromNow()

UI.registerHelper "prettifyDateFormat", (timestamp) ->
  moment(new Date(timestamp)).format('YYYY/MM/DD')

UI.registerHelper "compactDate", (timestamp) ->
  moment(new Date(timestamp)).format("H:mm:ss YYYY/MM/DD")

UI.registerHelper 'pluralize', (n, thing) ->
  # fairly stupid pluralizer
  if n == 0
    return ''
  if typeof n == 'undefined'
    return ''
  if n == 1
    '1 ' + thing
  else
    n + ' ' + thing + 's'

UI.registerHelper "truncateString", (string, length) ->
  truncate = string.substring(0,length)
  if string.length > length
    truncate += '...'
  truncate


UI.registerHelper "getActivitiesCounter", (trigger) ->
  if trigger.activities_counter?
    counter = trigger.activities_counter
  else
    counter = 0
  counter

UI.registerHelper "getNotificationIcon", (notification_type) ->
  if notification_type is 'danger'
    "fa-exclamation-circle text-red"
  else if notification_type is 'warning'
    "fa-warning text-yellow"
  else
    return "fa-info-circle text-aqua"

UI.registerHelper 'arrayify', (obj) ->
    result = []
    for key,value of obj
      result.push
        key:key
        value: value

    return result

UI.registerHelper "getTicketLabelColor", (state) ->
  if state is 'open'
    label = 'info'
  if state is 'processing'
    label = 'warning'
  if state is 'upgraded'
    label = 'danger'
  if state is 'closed'
    label = 'success'
  label

UI.registerHelper "divide", (x, y) ->
  if y isnt 0
    result = Math.round(x/y*10000)/10000

  else
    result = 0


UI.registerHelper "capitalize", (string) ->
  s(string).capitalize().value()

UI.registerHelper "isInServiceRole", (service, role) ->
  # {{#if isInServiceRole service 'business, admin, staff' }}
  comma = (role or '').indexOf(',')
  if comma != -1
    roles = _.reduce(role.split(','), ((memo, r) ->
      if !r or !r.trim()
        return memo
      memo.push r.trim()
      memo
    ), [])
  else
    roles = [ role ]

  user = Meteor.user()
  if user? and service?
    if user.profile[service].role in roles
      return true
  false

UI.registerHelper "getPackageSize", (role) ->
  size = 'col-md-4 col-xs-12'
  # if role is 'professional'
  #   size = 'col-md-3 col-xs-12'
  # if role is 'business'
  #   size = 'col-md-4 col-xs-12'
  # if role is 'premium'
  #   size = 'col-md-5 col-xs-12'
  size
