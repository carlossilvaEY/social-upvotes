@colorLuminance = (hex, lum) ->
  # validate hex string
  hex = String(hex).replace(/[^0-9a-f]/gi, '')
  if hex.length < 6
    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
  lum = lum or 0
  # convert to decimal and change luminosity
  rgb = '#'
  c = undefined
  i = undefined
  i = 0
  while i < 3
    c = parseInt(hex.substr(i * 2, 2), 16)
    c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16)
    rgb += ('00' + c).substr(c.length)
    i++
  rgb


@validateEmail = (email) ->
  re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
  re.test email


@colors =
  'twitter': ['#00c0ef', '#3c8dbc']
  'instagram' : ['#3c8dbc', '#00c0ef']
  'soundcloud': ['#F85E0F', '#F85E0F']


@drawCanvasLine = (data, element) ->
  drawElement = $("##{element}")
  ctx = $("##{element}").get(0).getContext("2d")
  myNewChart = new Chart(ctx)
  options =
    pointDot: true
    showScale: true
    scaleShowGridLines: false
    bezierCurveTension: 0.3
    bezierCurve: true
    responsive: true
    maintainAspectRatio: false
  new Chart(ctx).Line(data, options)

@drawSparkLine = (data, element, service) ->
  $("##{element}").sparkline data,
    type: 'line'
    width: '100%'
    height: '50'
    lineColor: '#000'
    fillColor: 'transparent'
