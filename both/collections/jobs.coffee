@QueueJobs = new JobCollection 'queue_jobs'

if Meteor.isServer
  QueueJobs.allow
    admin: (userId, method, params) ->
      loggedInUser = Meteor.user()
      if loggedInUser and Roles.userIsInRole(loggedInUser, ['admin', 'staff'])
        true

  @addJob = (jobName, data, delay = null) ->
    #newJob = QueueJobs.createJob(jobName, data)
    newJob = new Job(QueueJobs, jobName, data)

    retries = 10
    newJob.retry({retries: retries, wait: 60*1000})

    if delay?
      newJob.delay(delay)

    newJob.save()

  # we use it to restart the jobs that were running while the server restarted
  @cleanIdleJobs = () ->
    logger.debug 'cleanIdleJobs: called'

    jobsTypesToRemove = [
      "handleTwitterUserTriggers",
      "handleInstagramUserTriggers",
      "destroyTwitterFavorites"
    ]

    jobsToRemoveIds = _.pluck QueueJobs.find({
        status: 'running',
        type: { $in: jobsTypesToRemove }
      },
      {
        _id:1
      }
    ).fetch(), "_id"

    if jobsToRemoveIds.length
      logger.info "cleanIdleJobs: removing this jobs", jobsToRemoveIds:jobsToRemoveIds

    QueueJobs.cancelJobs jobsToRemoveIds # need to cancel before remove
    QueueJobs.removeJobs jobsToRemoveIds

    jobsToRestartIds = _.pluck QueueJobs.find({status: 'running', type: { $nin: jobsTypesToRemove } }, {_id:1}).fetch(), "_id"

    if jobsToRestartIds.length
      logger.info "cleanIdleJobs: restarting this jobs", jobsToRestartIds:jobsToRestartIds

    QueueJobs.cancelJobs jobsToRestartIds # need to cancel before restart
    QueueJobs.restartJobs jobsToRestartIds



  Meteor.methods
    clearCompletedJobs: ->
      logger.debug 'cleanCompletedJobs: called'
      jobsToRemoveIds = _.pluck QueueJobs.find({status: 'completed'}, {_id:1}).fetch(), "_id"
      QueueJobs.removeJobs jobsToRemoveIds

    cancelIdleJobs: ->
      logger.debug "cancelIdleJobs: called"
      thirty_mins_ago = new Date(moment().subtract(30, 'minutes').valueOf())
      jobsToCancel = QueueJobs.find
        status: 'running'
        updated: $lt: thirty_mins_ago
      ,
        _id:1
        # updated:1
      .fetch()

      jobsToCancelIds = _.pluck jobsToCancel, "_id"

      # logger.debug "cancelIdleJobs: data", jobsToCancelIds:jobsToCancelIds
      if jobsToCancelIds.length > 0
        logger.warn "cancelIdleJobs: found idle jobs to cancel", numJobs:jobsToCancelIds.length

      QueueJobs.cancelJobs jobsToCancelIds

    removeCancelledJobs: ->
      logger.debug 'removeCancelledJobs: called'
      jobsToRemoveIds = _.pluck QueueJobs.find({status: 'cancelled'}, {_id:1}).fetch(), "_id"
      QueueJobs.removeJobs jobsToRemoveIds
