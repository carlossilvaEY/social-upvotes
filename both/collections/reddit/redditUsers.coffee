@RedditUsers = new Meteor.Collection("reddit_users")

RedditUsers.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true
  remove: (userId, doc) ->
    !!userId


RedditUsersSchema = new SimpleSchema
  'username':
    unique: true
    type: String

RedditUsers.attachSchema RedditUsersSchema
