@RedditPosts = new Meteor.Collection("reddit_posts")

RedditPosts.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true
  remove: (userId, doc) ->
    !!userId


RedditPostsSchema = new SimpleSchema
  userId:
    type: String
  title:
    type: String
  #internal url in reddit
  url:
    type: String
    optional: true
  votes:
    type: Number
    optional: true
  writtenAt:
    type: Date
    optional: true
  updatedAt:
    type: Date
    autoValue: ()->
      new Date()

RedditPosts.attachSchema RedditPostsSchema
