@services = ['meneame', 'reddit']
@ServiceAccounts = new Mongo.Collection('service_accounts')

Schema = {}

ServiceAccounts.allow
  insert: (userId, doc) ->
    !!userId
  update: (userId, doc) ->
    userId and userId is doc.userId
  remove: (userId, doc) ->
    userId and Roles.userIsInRole userId, ['admin','staff']

ServiceAccounts.helpers
  getProxy: ()->
    proxy = Proxys.findOne @proxyId

    if proxy?
      return proxy.url()
    else
      return null

profileSchema = new SimpleSchema
  "username":
    type: String
    label: "Username"
    optional: true
  "email":
    type: String
    label: "Name"
    optional: true
  "userId":
    type: String
    label: "User Id"
    optional: true

ServiceAccountsSchema = new SimpleSchema
  "account":
    type: String
    label: "Account Name"
  "service":
    type: String
    label: "Service"
    allowedValues: services
    index: true
  "active":
    type: Boolean
    label: "Active"
    defaultValue: true
  "createdAt":
    type: Date
    label: "createdAt"
    autoValue: ()->
      new Date()
  "cookie":
    type: String
    label: "Cookie"
  "agent":
    type: String
    label: "Agent"
    optional: true
  "profile":
    type: profileSchema
    label: "Profile"
    optional: true
  "lastActivity":
    type: Date
    autoValue: ()->
      if @isInsert
        new Date()
  "proxyId":
    type: String
    label: "Proxy Id"
    optional: true

ServiceAccounts.attachSchema ServiceAccountsSchema

#createServiceAccountScheduleJob = (serviceAccountId) ->
#  serviceAccount = ServiceAccounts.findOne serviceAccountId
#  if !serviceAccount
#    throw new Meteor.Error(401, "Invalid service account id.")
#
#  Meteor.call "#{serviceAccount.service}CreateScheduledJob", serviceAccountId
#
#Meteor.methods
#  'toogleServiceAccount': (serviceAccountId, value) ->
#    check(serviceAccountId, String)
#    check(value, Boolean)
#
#    userId = Meteor.userId()
#    if !userId
#      throw new Meteor.Error(401, "You need to login")
#
#    # if value is true
#    #   if Triggers.find({serviceAccountId:serviceAccountId, active:true }).count() is 0
#    #     throw new Meteor.Error(401, "Don't have any Tag added.")
#    #   else
#    #     createServiceAccountScheduleJob serviceAccountId
#
#    updateOptions =
#      $set:
#        'active': value
#
#    ServiceAccounts.update serviceAccountId, updateOptions
#
#if Meteor.isServer
#  @checkScheduledServiceAccount = (serviceAccountId) ->
#    serviceAccount = ServiceAccounts.findOne serviceAccountId
#    if !serviceAccount
#      logger.warn "checkScheduledServiceAccount . The service account does not exist", serviceAccountId:serviceAccountId
#      return false
#
#    if !serviceAccount.isSubscribed
#      logger.warn "checkScheduledServiceAccount . The service account is not subscribed", serviceAccountId:serviceAccountId
#      return false
#
#    if serviceAccount.isDeleted
#      logger.warn "checkScheduledServiceAccount . The service account is deleted (no active)", serviceAccountId:serviceAccountId
#      return false
#
#    # TODO - check valid token
#
#    if serviceAccount.service is 'instagram'
#      # check checkpoint required
#      unless serviceAccount.igCookie?
#        logger.warn "checkScheduledServiceAccount. The service has no cookie ", serviceAccountId:serviceAccountId
#        return false
#
#      if serviceAccount.checkpointRequired? and serviceAccount.checkpointRequired is true
#        logger.warn "checkScheduledServiceAccount. The service has checkpoint required ", serviceAccountId:serviceAccountId
#        return false
#
#    true
#
#  @createServiceAccount = (userId, service, params) ->
#
#    exits = ServiceAccounts.findOne serviceId:params.serviceId, service:service
#
#    if exits
#      text = if exits.userId is userId then "by you" else "by other user"
#      throw new Meteor.Error 401, "This account has been already added #{text}"
#
#    account =
#      userId: userId
#      service: service
#      serviceId: params.serviceId
#      profile: params.profile
#      createdAt: new Date()
#
#    if service is 'instagram'
#      account.igCookie = params.cookie
#      account.agent = params.agent
#
#    if service is 'twitter'
#      account.twAuthToken = params.authToken
#
#    ServiceAccounts.insert account
#
#  #Conversions
#  @handleAccountServiceConversions = (serviceAccountId) ->
#    date = today()
#    logger.debug "#{service}: handleAccountServiceConversions: called", serviceAccountId:serviceAccountId, date:date
#    newFollowers = getNewFollowersByDate serviceAccountId, date
#
#    for authorId in newFollowers
#      findOptions =
#        'author.authorId': authorId
#        'serviceAccountId': serviceAccountId
#
#      activity = Activities.findOne(findOptions)
#      if activity?
#        if !Conversions.findOne({authorId:authorId, serviceAccountId:serviceAccountId})
#          # We create the conversion
#          createConversion(activity.author.authorId, activity.author.name, activity.author.thumb, serviceAccountId, activity.triggerId )
#
#    logger.info "#{service} handleServiceConversions: success", serviceAccountId:serviceAccountId, date:date
#    serviceAccountId
#
#  getNewFollowersByDate = (serviceAccountId, date) ->
#    dateString = date
#    dayBeforeString = dayBefore(date)
#
#    today_stats = ServiceAccountStats.findOne({serviceAccountId:serviceAccountId, date:dateString})
#    yesterday_stats = ServiceAccountStats.findOne({serviceAccountId:serviceAccountId, date:dayBeforeString})
#    difference = []
#    if today_stats? and yesterday_stats?
#      if today_stats?.last_followers? and yesterday_stats?.last_followers?
#        today_followers = today_stats.last_followers
#        logger.debug "getNewFollowersByDate", today_followers:today_followers.length
#        yesterday_followers = yesterday_stats.last_followers
#        logger.debug "getNewFollowersByDate", yesterday_followers:yesterday_followers.length
#        if today_followers and yesterday_followers
#          difference = _.difference(today_followers, yesterday_followers)
#          logger.debug "getNewFollowersByDate", serviceAccountId:serviceAccountId, difference:difference.length
#        else
#          unless today_followers?
#            logger.warn "getNewFollowers: today_followers empty", serviceAccountId:serviceAccountId, service:service
#          unless yesterday_followers?
#            logger.warn "getNewFollowers: yesterday_followers empty", serviceAccountId:serviceAccountId, service:service
#    difference
#
