@action = ['upvote', 'downvote']

@Activities = new Meteor.Collection("activities")

Activities.allow
  insert: (userId, doc) ->
    return userId and Roles.userIsInRole userId, ['admin','staff']
  update: (userId, doc) ->
    return userId and Roles.userIsInRole userId, ['admin','staff']
  remove: (userId, doc) ->
    return userId and Roles.userIsInRole userId, ['admin','staff']


ActivitiesSchema = new SimpleSchema
  serviceAccount: #related id
    type: String
  action:
    type: String
    allowedValues: action
  date:
    type: Date
    autoValue: ()->
      if @isInsert
        new Date()
  #campos relacionados con la web donde se hizo la accion
  url:
    type: String
    optional: true
  username: #username with action
    type: String
    optional: true
  actionId: #id external
    type: String
    optional: true

Activities.attachSchema ActivitiesSchema
