@Stats = new Meteor.Collection("stats")

Stats.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true
  remove: (userId, doc) ->
    !!userId


StatsSchema = new SimpleSchema
  'answer_id':
    type: String
    index: true
  'username':
    type: String
    index: true
  'votes':
    type: Number
  'views':
    type: Number
  'position':
    type: Number
  'createdAt':
    type: Date
  'date':
    type: String

Stats.attachSchema StatsSchema



@today = () ->
	now = new Date()
	month = now.getMonth() + 1
	year = now.getFullYear()
	day = now.getDate()
	date = year + "-" + month + "-" + day
	date

@yesterday = () ->
  now = new Date()
  yesterday = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1)
  month = yesterday.getMonth() + 1
  year = yesterday.getFullYear()
  day = yesterday.getDate()
  date = year + "-" + month + "-" + day
  date

@lastWeek = () ->
  now = new Date()
  last_week = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7)
  month = last_week.getMonth() + 1
  year = last_week.getFullYear()
  day = last_week.getDate()
  date = year + "-" + month + "-" + day
  date
