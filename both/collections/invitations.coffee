@Invitations = new Meteor.Collection("invitations")

Invitations.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true
  remove: (userId, doc) ->
    !!userId

InvitationsSchema = new SimpleSchema
  email:
    type: String
    unique: true
  message:
    type: String
    optional: true
  company:
    type: String
    optional: true
  active:
    type: Boolean
    defaultValue: true
  createdAt:
    type: Date
    autoValue: ()->
      if @isInsert
        new Date()
  reg_code:
    type: String
    optional: true

Invitations.attachSchema InvitationsSchema
