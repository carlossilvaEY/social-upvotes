@UserStats = new Meteor.Collection("user_stats")

UserStats.allow
  insert: (userId, doc) ->
    true
  update: (userId, doc) ->
    true
  remove: (userId, doc) ->
    !!userId


UserStatsSchema = new SimpleSchema
  'username':
    index: true
    type: String
  'answers':
    type: Number
  'questions':
    type: Number
  'posts':
    type: Number
  'followers':
    type: Number
  'following':
    type: Number
  'createdAt':
    type: Date
  'date':
    type: String

UserStats.attachSchema UserStatsSchema
