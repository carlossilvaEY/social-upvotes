@objectToCookie = (obj) ->
  cookieString = ''
  for key, value of obj
    cookieString += "#{key}=#{value}; "
  cookieString.slice(0, -2)


@checkServiceUrl = (url, type)->
  re = new RegExp("(((http|https):\/\/)|(\/)|())+(www.|())+(#{type})", "g")
  m = re.exec(url)

  if m?[0]?
    return true
  else
    return false
