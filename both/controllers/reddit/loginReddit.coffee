@getRamdomProxy = ()->
  proxys = Proxys.find "active" : true, 'available': true
  randomIndex = Math.floor(Math.random() * proxys.count())
  proxy = proxys.fetch()[randomIndex]
  return proxy

if Meteor.isServer
  Meteor.methods
    'redditLogin': (username, passwd) ->

      optionsProxy = getRamdomProxy()

      if optionsProxy?
        proxyId = optionsProxy._id
      else
        proxyId = null

      loginDataString = "op=login&user=#{username}&passwd=#{passwd}&api_type=json"

      headers =
        'Host': 'www.reddit.com'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0'
        'Content-Type':	'application/x-www-form-urlencoded; charset=UTF-8'
        'X-Requested-With':	'XMLHttpRequest'
        'Referer':	'https://www.reddit.com/'
        'Connection':	'keep-alive'
        'Accept':'application/json, text/javascript, */*; q=0.01'
        'Accept-Language': 'en-US,en;q=0.5'
        'Referer': 'https://www.reddit.com/'

      options =
        method: 'POST'
        body: loginDataString
        url: "https://www.reddit.com/api/login/#{username}"
        headers: headers
        gzip: true
        rejectUnauthorized: false
        followAllRedirects: true

      if optionsProxy?
        options['proxy'] = optionsProxy.url()

      result = requestAsync options
      raw = result.cookieResponse._jar.store.idx['reddit.com']['/']

      cfduid = raw['__cfduid']
      loid = raw['loid']
      loidcreated = raw['loidcreated']
      eu_cookie_v2=2;
      reddit_session = raw['reddit_session']
      secure_session = raw['secure_session']
      pc = 'bv'

      cookie = "#{cfduid}; #{loid}; #{loidcreated}; #{eu_cookie_v2}; #{reddit_session}; pc=bv;"

      service = ServiceAccounts.findOne({account: username, service: services[1]})

      if !service
        message = "Insert account #{username}"
        profile =
          username: username

        serviceId = ServiceAccounts.insert
          account: username
          service: services[1]
          cookie: cookie
          agent: headers['User-Agent']
          profile: profile
          proxyId: proxyId
      else
        message = "Update account #{username}"

        objectUpdate =
          $set:
            service: services[1]
            cookie: cookie
            agent: headers['User-Agent']
            profile: profile
            proxyId: proxyId

        ServiceAccounts.update service._id, objectUpdate
        serviceId = service._id

      proxyUpdate =
        $addToSet:
          users: serviceId

      Proxys.update proxyId, proxyUpdate

      return {error: null, message: message}
