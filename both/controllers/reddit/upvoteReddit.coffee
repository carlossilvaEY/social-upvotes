if Meteor.isServer
  Meteor.methods
    'redditDataUrl': (accountId, url)->
      account = ServiceAccounts.findOne accountId

      cookie = account.cookie

      headers =
        'Host': 'www.reddit.com'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0'
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Referer': 'https://www.reddit.com/'
        'Cookie': cookie
        'Connection': 'keep-alive'

      options =
        method: 'GET'
        url: url
        headers: headers
        gzip: true
        rejectUnauthorized: false
        followAllRedirects: true
        proxy: account.getProxy()

      result = requestAsync options
      body = result.content

      whereisVoteHash = body.indexOf('vote_hash')
      voteHast = body.substring(whereisVoteHash + 13, whereisVoteHash+185)

      whereisModhash = body.indexOf('modhash')
      modhash = body.substring(whereisModhash + 11, whereisModhash + 61)

      $ = cheerio.load(body)
      divId = $('div#siteTable').html()

      re = /id-[a-zA-Z0-9_]*/
      m = re.exec(divId)

      if m?[0]?
        id = m[0].replace('id-', '')

      result =
        error: null
        storyId: id
        voteHast: voteHast
        modhash: modhash

      return result

    'redditUpvote': (accountId, url)->
      account = ServiceAccounts.findOne accountId
      cookie = account.cookie

      headers =
        'Host': 'www.reddit.com'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0'
        'Accept': 'application/json, text/javascript, */*; q=0.01'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        'X-Requested-With': 'XMLHttpRequest'
        'Referer': 'https://www.reddit.com/'
        'Cookie': cookie
        'Connection': 'keep-alive'

      data = Meteor.call 'redditDataUrl', account._id, url

      id = data.storyId
      vote_hash = data.vote_hash
      modhash = data.modhash

      dataString = "id=#{id}&dir=1&vh=#{vote_hash}&isTrusted=true&rank=2&uh=#{modhash}&renderstyle=html"

      options =
        method: 'POST'
        body: dataString
        url: "https://www.reddit.com/api/vote"
        headers: headers
        gzip: true
        rejectUnauthorized: false
        followAllRedirects: true
        proxy: account.getProxy()

      result = requestAsync options
      console.log "response reddit: #{result.content}"
      return {error: result.error, result: result.content}
