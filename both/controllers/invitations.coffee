Meteor.methods
  'addInvitation': (params) ->

    console.log 'add Invitations'

    check(params, {email: String,createdAt: Date, active: Boolean})

    if params.email?
      try
        Invitations.insert(params)
      catch e
        throw new Meteor.Error(403, "There was an error:#{e}")

      true
    else
      false
