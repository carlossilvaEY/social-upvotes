addJobsToUpvotes = (upvotes, service, url, data)->

  accounts = ServiceAccounts.find({service: service}).fetch()
  upvoted = 0
  delay = 0
  for account in accounts

    activity = Activities.findOne({
                                account: account._id,
                                action: action[0],
                                url: url,
                                actionId: data.storyId
                                },
                                {fields:{_id: 1}})
    if !activity
      delay = delay + _.random(90000,1800000)

      console.log "job name: #{service}UpVote"

      addJob "#{service}UpVoteJob", {account: account._id, url: url}, delay
      upvoted += 1

      #save activity in db
      Activities.insert({
                        serviceAccount: account._id,
                        action: action[0],
                        url: url,
                        actionId: data.storyId
                        })


    if upvoted == upvotes
      break

  return


if Meteor.isServer
  Meteor.methods
    upvoteDownvote: (url, upvotes, downvotes, service)->
      upvotes = if upvotes then parseInt(upvotes) else 0

      account = ServiceAccounts.findOne({service: service})
      data = Meteor.call "#{service}DataUrl", account._id, url

      if data.error
        return {error: data.error}

      #console.log data
      addJobsToUpvotes(upvotes, service, url, data)

      return {error: null, message: "Add Jobs #{service}"}

    updateStateAccount: (accountId, active) ->
      active = if active then false else true

      updateObject =
        $set:
          'active': active

      ServiceAccounts.update accountId, updateObject

    deleteAccount: (accountId) ->
      ServiceAccounts.remove accountId
