@meneameGetBaseKey = (body) ->
  re = /var base_key="[a-zA-Z0-9\/-]{51}/
  #random string
  base_key = '1457014956-96e134327b26571062b9f4b8828b4dc18e488f18'
  m = re.exec(body)
  if m?
    base_key = m[0].replace('var base_key="', '')
  return base_key


#return username and userID
getUserInfo = (content)->
  $ = cheerio.load(content)
  headerTop = $('div#header-top').find('ul#userinfo')
  blockUsername = headerTop.find('li.usertext.wideonly')#.find('a')

  username = blockUsername.find('a').attr('href').replace('/user/', '')

  re = /u:[0-9]*/
  if (m = re.exec(blockUsername.html()))?
    userId = m[0].replace('u:', '')

  userData =
    userId: userId
    username: username

  return userData

getLoginError = (content)->
  $ = cheerio.load(content)
  error = $('div.form-error').text()
  return error

setProxy = ()->
  proxys = Proxys.find({active: true, available: true}).fetch()
  for proxy in proxys
    serviceAccount = ServiceAccounts.find proxyId: proxy._id, service: services[0]
    if serviceAccount.count() == 0
      return proxy

  return null


if Meteor.isServer
  Meteor.methods
    'meneameLogin': (email, password)->

      optionsProxy = setProxy()

      if optionsProxy?
        proxyId = optionsProxy._id
      else
        proxyId = null

      headers =
        'Host': 'www.meneame.net'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0'
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Connection': 'keep-alive'

      options =
        url: 'https://www.meneame.net/login?return=%2F'
        headers: headers
        gzip: true
        rejectUnauthorized: false
        followAllRedirects: true

      if optionsProxy?
        options['proxy'] = optionsProxy.url()

      response = requestAsync options

      base_key = meneameGetBaseKey(response.content)

      cookie =
        'k': "#{base_key}"
        'ads_bm_last_load_status': 'BLOCKING'
        'bm_monthly_unique': 'true'
        'bm_daily_unique':'true'
        'bm_sample_frequency':'1'
        'return_site':'https://www.meneame.net'
        'bm_last_load_status':'BLOCKING'

      headers =
        'Host': 'www.meneame.net'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0'
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Referer': 'https://www.meneame.net/login'
        'Connection': 'keep-alive'
        'Content-Type': 'application/x-www-form-urlencoded'

      loginDataString = "username=#{email}&password=#{password}&processlogin=1&return=%2F"

      url = 'https://www.meneame.net/login'

      options =
        url: url
        headers: headers
        body: loginDataString
        rejectUnauthorized: false
        followAllRedirects: true
        gzip: true
        method: 'POST'

      if optionsProxy?
        options['proxy'] = optionsProxy.url()

      response = requestAsync options

      loginError = getLoginError(response.content)

      if loginError
        return {error: loginError}

      headers = response.headers['set-cookie']
      k = headers[1].replace('path=/', '')
      awselb = headers[2].replace('PATH=/', '')

      u = response.cookieResponse._jar.store.idx['meneame.net']['/']['u']
      ukey = response.cookieResponse._jar.store.idx['meneame.net']['/']['ukey']


      cookie = "#{k}#{awselb}#{u}; #{ukey}"
      userInfo = getUserInfo(response.content)

      service = ServiceAccounts.findOne({account: email, service: services[0]})

      if !service
        message = "Insert account #{email}"
        profile =
          username: userInfo.username
          email: email
          userId: userInfo.userId

        serviceId = ServiceAccounts.insert
                                        account: email
                                        service: services[0]
                                        cookie: cookie
                                        agent: headers['User-Agent']
                                        profile: profile
                                        proxyId: proxyId

      else
        message = "Update account #{email}"

        profile =
          username: userInfo.username
          email: email
          userId: userInfo.userId

        objectUpdate =
          $set:
            service: services[0]
            cookie: cookie
            agent: headers['User-Agent']
            profile: profile
            proxyId: proxyId

        serviceId = service._id
        ServiceAccounts.update serviceId, objectUpdate

      proxyUpdate =
        $addToSet:
          users: serviceId

      Proxys.update proxyId, proxyUpdate
      return {error: null, message: message}
