getStoryId = (content)->
  $ = cheerio.load(content)

  storyId = $('div.news-body').find('div.menealo').attr('id')

  if storyId?
    storyId = storyId.replace('a-va-', '')
  else
    storyId = null

  return storyId


if Meteor.isServer
  Meteor.methods
    'meneameDataUrl': (accountId, url)->
      account = ServiceAccounts.findOne accountId

      baseCookie =
        'a':	1
        'sticky':	1

      cookie = account.cookie + objectToCookie baseCookie

      headers =
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Referer': 'https://www.meneame.net/'
        'Connection': 'keep-alive'
        'Cookie': cookie
        'host': 'www.meneame.net'

      headers['User-Agent'] = account.agent

      options =
        url: url
        headers: headers
        rejectUnauthorized: false
        followAllRedirects: true
        gzip: true
        method: 'GET'
        proxy: account.getProxy()

      response = requestAsync(options)
      body = response.content

      if response.error
        result = {error: response.error}
      else
        result = {
          error: null,
          storyId: getStoryId(body),
          key: meneameGetBaseKey(body),
          }

      return result


    'meneameUpvote': (accountId, url)->

      account = ServiceAccounts.findOne accountId
      data = Meteor.call 'meneameDataUrl', account._id, url

      if data.error
        return {error: data.error}

      id = data.storyId
      key = data.key
      user = account.profile.userId

      #up visit
      headers =
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Referer': 'https://www.meneame.net/'
        'Connection': 'keep-alive'
        'Cookie': account.cookie
        'host': 'www.meneame.net'

      headers['User-Agent'] = account.agent

      options =
        url: "https://www.meneame.net/go?quiet=1&id=#{id}"
        headers: headers
        rejectUnauthorized: false
        followAllRedirects: true
        gzip: true
        method: 'GET'
        proxy: account.getProxy()

      requestAsync(options)

      #meneo
      url = "https://www.meneame.net/backend/menealo?id=#{id}&user=#{user}&key=#{key}&l=0&u=https%3A%2F%2Fwww.meneame.net%2Flogin%3Freturn%3D%252F&_=1458162028280"

      baseCookie =
        sticky: 1
        a: 1
        Connection: 'keep-alive'

      cookie = account.cookie + objectToCookie baseCookie
      #console.log cookie

      #up to visit
      headers =
        'Host': 'www.meneame.net'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0'
        'Accept': 'application/json, text/javascript, */*; q=0.01'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'X-Requested-With': 'XMLHttpRequest'
        'Referer': 'https://www.meneame.net/'
        'Cookie': cookie

      options =
        url: url
        headers: headers
        rejectUnauthorized: false
        followAllRedirects: true
        gzip: true
        method: 'GET'
        proxy: account.getProxy()

      response = requestAsync options

      console.log response.content

      return response.content
