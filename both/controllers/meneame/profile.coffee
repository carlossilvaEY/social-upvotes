getPosts = ($, user)->
  $('div#siteTable').children('.link').each (i, element) ->
    item = $(this)
    post =
      'votes': item.find('div.midcol.unvoted').find('div.score.unvoted').text()
      'title': item.find('div.entry').find('p.title').text()
      'created': item.find('div.entry').find('p.tagline').find('time').attr('datetime')
      'url': item.find('div.entry').find('a.comments').attr('href')

    obj = RedditPosts.findOne title: post.title

    #console.log obj
    if obj
      updateObject =
        $set:
          votes: post.votes

      RedditPosts.update obj._id, updateObject
    else
      RedditPosts.insert({
                          userId: user._id
                          title: post.title,
                          url: post.url,
                          votes: post.votes,
                          writtenAt: post.created
                          })

if Meteor.isServer
  Meteor.methods
    'getAllUserPosts': (account, username)->

      user = RedditUsers.findOne username: username

      headers =
        'Host': 'www.reddit.com'
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0'
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        'Accept-Language': 'en-US,en;q=0.5'
        'Accept-Encoding': 'gzip, deflate, br'
        'Connection': 'keep-alive'

      options =
        url: "https://www.reddit.com/user/#{username}/submitted/"
        headers: headers
        gzip: true
        rejectUnauthorized: false
        followAllRedirects: true
        method: 'GET'

      if account.proxy
        options['proxy'] = account.proxy

      next = true
      firstLoop = true
      while next
        response = requestAsync options

        if response.error
          console.log response.error

        body = response.content
        $ = cheerio.load(body)

        getPosts($, user) #save posts

        paginator = $('div.nav-buttons')

        if paginator
          if firstLoop
            nextAndPreview = paginator.find('a').attr('href')
            firstLoop = false
          else
            nextAndPreview = paginator.find('a').last().attr('href')

          re = /(after=)/
          m = re.exec(nextAndPreview)

          if m?[0]?
            nextUrl = nextAndPreview
            options['url'] = nextUrl
          else
            next = false
        else
          next = false
