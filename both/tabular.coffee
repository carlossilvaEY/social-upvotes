TabularTables = {}
Meteor.isClient and Template.registerHelper('TabularTables', TabularTables)

TabularTables.UsersAdmin = new Tabular.Table
  name: 'UsersAdmin'
  collection: Meteor.users
  pub: "tabular_Users",
  allow:  (userId) ->
    return Roles.userIsInRole userId, ['admin','staff']
  pageLength: 25
  extraFields: ['roles']
  autoWidth: false
  order: [[1, "desc"]],
  columns: [
    {
      title: "Name"
      data: "username"
      tmpl: Meteor.isClient && Template.tabularUserName
    }
    {
      title: "Email"
      data:"emails.0.address"
    }
    {
      title: "Since"
      data: "createdAt"
      render: (val, type, doc) ->
        moment(new Date(val)).fromNow()
    }
    {
      data: "_id"
      visible: false
    }
  ]

TabularTables.JobsAdmin = new Tabular.Table
  name: 'JobsAdmin'
  collection: QueueJobs
  pub: "tabular_Jobs",
  allow:  (userId) ->
    return Roles.userIsInRole userId, 'admin'
  pageLength: 25
  extraFields: ['retries', "depends", 'resolved', 'repeated', 'repeats',
                'status', 'progress', 'log'
  ]
  autoWidth: false
  order: [[3, "desc"]],
  columns: [
    {
      title: "Type"
      data: "type"
    }
    {
      title: "ID"
      data: "_id"
    }
    {
      title: "UserId"
      data: "data.userId"
    }
    {
      title: "Run at"
      data: "after"
      render: (val, type, doc) ->
        output = moment(val).fromNow()
        if val > new Date()
          "<span class='text-danger'>#{output}</span>"
        else
          "<span class='text-success'>#{output}</span>"

    }
    {
      title: "Updated"
      data: "updated"
      render: (val, type, doc) ->
        moment(val).fromNow()
    }
    {
      title: "Priority"
      data: "priority"
    }
    {
      title: "Dep"
      # data: "depends"
      render: (val, type, doc) ->
        numDepends = doc.depends?.length or 0
        numResolved = doc.resolved?.length or 0
        "#{numDepends} / #{numResolved}"
    }
    {
      title: "Attempts"
      data: "retried"
      render: (val, type, doc) ->
        numRetries = if doc.retries is QueueJobs.Forever then "∞" else doc.retries
        "#{doc.retried} / #{numRetries}"
    }
    {
      title: "Repeats"
      data: "repeated"
      render: (val, type, doc) ->
        numRepeats = if doc.repeats is QueueJobs.Forever then "∞" else doc.repeats
        "#{doc.repeated} / #{numRepeats}"
    }
    {
      title: "Status"
      data: "status"
      tmpl: Meteor.isClient && Template.tabularJobStatus
    }
    {
      title: "Actions"
      tmpl: Meteor.isClient && Template.tabularJobActions
    }


  ]


TabularTables.servicesAccounts = new Tabular.Table
  name: 'servicesAccounts'
  collection: ServiceAccounts
  pageLength: 25
  autoWidth: false
  order: [[1, "desc"]],
  extraFields: ['account', 'service', 'active']
  columns: [
    {
      title: "Account"
      tmpl: Meteor.isClient and Template.accountName
    }
    {
      title: "Proxy"
      data: "proxyId"
    }
    {
      title: "Last activity"
      data: "lastActivity"
      render: (val, type, doc) ->
        moment(val).fromNow()
    }
    {
      title: "Active"
      data: "active"
    }
    {
      title: "Actions"
      tmpl: Meteor.isClient and Template.servicesAccountsActions
    }
  ]


TabularTables.servicesAccountsActivities = new Tabular.Table
  name: 'servicesAccountsActivities'
  collection: Activities
  pageLength: 25
  autoWidth: false
  order: [[1, "desc"]],
  extraFields: []
  columns: [
    {
      title: "Action"
      data: "action"
    }
    {
      title: "Date"
      data: "date"
      render: (val, type, doc) ->
        moment(val).fromNow()
    }
    {
      title: "Url"
      data: "url"
    }
    {
      title: "User to Action"
      data: "username"
    }
    {
      title: "Action Id (Id Story)"
      data: "actionId"
    }
  ]




TabularTables.InvitationsAdmin = new Tabular.Table
  name: 'InvitationsAdmin'
  collection: Invitations
  pub: "tabular_Invitations",
  # allow:  (userId) ->
  #   return Roles.userIsInRole userId, 'admin'
  pageLength: 25
  extraFields: []
  autoWidth: false
  order: [[3, "desc"]],
  columns: [
    {
      title: "Email"
      data: "email"
    }
    {
      title: "Company"
      data: "company"
    }
    {
      title: "Message"
      data: "message"
    }
    {
      title: "Since"
      data: "createdAt"
      render: (val, type, doc) ->
        moment(new Date(val)).fromNow()
    }
    {
      title: "Registration Code"
      data: "reg_code"
      tmpl: Meteor.isClient && Template.tabularRegistrationCode
    }
    {
      title: "Active"
      data: "active"
    }
  ]
