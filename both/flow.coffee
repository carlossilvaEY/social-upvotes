# not found routes
FlowRouter.notFound =
  name: "notFound"
  action: () ->
    BlazeLayout.render('layout', main: '404')

# login and signup config
AccountsTemplates.configure
  defaultLayoutType: 'blaze'
  defaultLayout: 'layoutLogin'
  defaultLayoutRegions:
    nav: 'header'
  defaultContentRegion: 'main'
  homeRoutePath: 'dashboardHome'
  onLogoutHook: () -> FlowRouter.go '/sign-up'

AccountsTemplates.configureRoute 'signIn',
  name: 'signIn'
  path: '/sign-in'
  template: 'signIn'

AccountsTemplates.configureRoute 'signUp',
  name: 'signUp'
  path: '/sign-up'
  template: 'signIn'

# home
FlowRouter.route '/',
  name: 'homePage'
  action: () ->
    BlazeLayout.render('homeLayout')


FlowRouter.route '/ask-invitation',
  name: 'askInvitation'
  action: () ->
    BlazeLayout.render('accounts_layout', {
      main: 'askInvitation',
      })

# dashboard routes
cleanBodyClass = () ->
  $('body').removeClass("hold-transition register-page")
  $('body').addClass("sidebar-mini skin-black fixed")

dashboardRoutes = FlowRouter.group
  name: 'dashboard'
  triggersEnter: [AccountsTemplates.ensureSignedIn, cleanBodyClass]

dashboardRoutes.route '/dashboard/',
  name: 'dashboardHome'
  action: () ->
    BlazeLayout.render('layoutDashboard', {
      main: 'dashboardHome',
      header: 'header',
      sideMenu: 'sideMenu'
      })



# ADMIN ROUTES
ensureAdminRole = () ->
	unless Roles.userIsInRole Meteor.userId(), [ 'admin' ]
		FlowRouter.go 'dashboardHome'

#services
dashboardRoutes.route '/accounts/:service/add',
  name: 'serviceAddUser',
  action: (params, queryParams)->
    BlazeLayout.render('layoutDashboard', {main: 'serviceAddUser', header: 'header', sideMenu: 'sideMenu'})

dashboardRoutes.route '/accounts/:service/actions',
  name: 'upvoteDownvote',
  action: (params, queryParams)->
    BlazeLayout.render('layoutDashboard', {main: 'upvoteDownvote', header: 'header', sideMenu: 'sideMenu'})

dashboardRoutes.route '/accounts/:service',
  name: 'servicesAccounts',
  action: (params, queryParams)->
    BlazeLayout.render('layoutDashboard', {main: 'servicesAccounts', header: 'header', sideMenu: 'sideMenu'})

dashboardRoutes.route '/accounts/:service/activities/:account',
  name: 'serviceAccountActivities',
  action: (params, queryParams)->
    BlazeLayout.render('layoutDashboard', {main: 'serviceAccountActivities', header: 'header', sideMenu: 'sideMenu'})

#admin routes
adminRoutes = FlowRouter.group
	prefix: '/admin'
	name: 'admin'
	triggersEnter: [AccountsTemplates.ensureSignedIn, cleanBodyClass, ensureAdminRole]

adminRoutes.route '/users/',
  name:'adminUsers'
  action: () ->
    BlazeLayout.render('layoutDashboard', {main: 'adminUsers', header: 'header', sideMenu: 'sideMenu'})

adminRoutes.route '/support/',
  name:'adminSupport'
  action: () ->
    BlazeLayout.render('layoutDashboard', {main: 'adminSupport', header: 'header', sideMenu: 'sideMenu'})

adminRoutes.route '/proxys/',
  name:'adminProxys'
  action: () ->
    BlazeLayout.render('layoutDashboard', {main: 'adminProxys', header: 'header', sideMenu: 'sideMenu'})

adminRoutes.route '/invitations/',
  name:'adminInvitations'

  #subscriptions: ->
  #  @register invitationsCounter, Meteor.subscribe 'invitationsCounter'

  action: () ->
    BlazeLayout.render('layoutDashboard', {main: 'adminInvitations', header: 'header', sideMenu: 'sideMenu'})



adminRoutes.route '/jobs/',
  name:'adminJobs'
  action: () ->
    BlazeLayout.render('layoutDashboard', {main: 'adminJobs', header: 'header', sideMenu: 'sideMenu'})
